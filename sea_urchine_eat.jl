# using Pkg
# Pkg.add("StatsBase")
using Printf
using StatsBase

#=
So what i need is the height of the kelp forrest,
the area it takes up and the amount of kelp a sea
urchin eats in a day.

For this example I will assume that the kelp takes 
up most of the volume of a kelp forrest for 
the sake of simplicity

since variables are declared outside of while loop 
tjeu meed tp be labelled as global inside the loop.
Same goes for for loop.
=#



############### initial params ##############

kelp_area = 12*100000 # sqr feet

kelp_height = 12*rand(100:175) # feet

kelp_forrest = kelp_height*kelp_area # cubic feet

num_urchins = 100 # need to refine

ind_urch_intake = 20 # Need to refine

total_urch_intake = num_urchins*ind_urch_intake

days = 0 # this will measure how long it takes

weighted_days = 0

############################################


regen_rate = rand(11:24) # 11-24 inches per day

#@printf("%F", regen_rate,"\n")
#println(regen_rate,"\n")

while kelp_forrest > 0
    global kelp_forrest -= total_urch_intake 
    global days += 1
    global kelp_forrest += regen_rate
    # println(days)
    
end
# println("Days before eaten without weighted average: ",days)

#=
The below section will use a weighted average instead of a 
randomized pick to see how results differ
print contents of an array

A = []
for i in eachindex(A)
    println(i)
end
=#

# collect() is basically range() in python?

reg_rate = collect(11:24)
weighted_reg = [.01, .02, .05, .06, .075,
           .11, .125, .15, .125, .11,
           .075, .06, .02, .01]

# gives a weighted average to our regeneration weight
reg_sample = sample(reg_rate, Weights(weighted_reg))

kelp_forrest = kelp_height * kelp_area

while kelp_forrest > 0
    global kelp_forrest -= total_urch_intake 
    global weighted_days += 1
    global kelp_forrest += reg_sample
    
end
# println("\nDays before eaten with weighted average: ",weighted_days)
#println("\n",reg_sample)

diff_days = abs(weighted_days - days)

println("The difference between the two is ", diff_days, " days.")
