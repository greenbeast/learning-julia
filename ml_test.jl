# using Pkg
# Pkg.add("Flux")
# Only needs to be run once to install Flux.jl
# https://www.math.purdue.edu/~allen450/ML-with-Julia-Tutorial.html
using Flux
using Flux.Zygote
using Printf

# Fucking differentiation apparently
# executable math

f(x) = x^2+1

# f'(x) = 2x
df(x) = gradient(f,x)[1] # df is a tuple, [1] gets the first coordinate

der = df(4)
# @printf("%f", der)

# f''(x) = 2
ddf(x) = gradient(df,x)[1]

#@printf("%f", ddf(0))

# Have fun deriving this...
h(x) = -cos(x)^cos(x)

dh(x) = gradient(h,x)[1]
# @printf("%f",dh(pi/4))

f(x,y,z) = x^2 + y^2 + z^2

grad(f) = gradient(f,1,2,3)

# Introducing Params: linear regression

# random initial params
w = rand(5,10)
b = rand(5)

fhat(x) = w*x + b

function loss(x,y)
    yhat = fhat(x)
    return sum((y-yhat).^2)
end

x = rand(10)
y = rand(5)

# @printf("%f", loss(x,y))

#=
This is how a code chunk is 
done is julia. So that is 
something that is nice to know
=#



println("\nFinished compiling")
