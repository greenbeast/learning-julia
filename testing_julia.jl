using Pkg
using PyPlot
using PyCall
@pyimport numpy

ar1 = numpy.random.random_sample((5,))
ave = numpy.mean(ar1)
println(ave)

x = 30
for i in 1:40
    x += 1
end

println(x)

@time begin
    x = 30
    for i in 1:40
        x += i
    end
end

function sphere(r)
    return 4/3*pi*r^3
end

quad(a, sqr, b) = (-b+sqr)/(2a)

function quad_form(a,b,c)
    sqr = sqrt(b^2-4ac)
    r1 = quad(a,sqr,b)
    r2 = quad(a,-sqr,b)
end

quad_form(2,-2,-12)

println("Quadratic formula output for (2,-2,-12): $(quad_form(2,-2,-12))")
